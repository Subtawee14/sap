<?php
session_start();
include 'connection.php';
$style = "";
$styleAdmin = "style='display:none;'";
$styleSignout = "style='display:none;'";
if (isset($_SESSION['UserID'])) {
    $style = "style='display:none;'";
    $styleSignout = "";

    $sql = "select sum(Item_price) as `sum`
                                from cart,items
                                where User_id =" . $_SESSION["UserID"] . " AND cart.Item_id = items.ID";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $_SESSION["TotalPrice"] = $row["sum"];
        session_write_close();
    }

}
if(isset($_SESSION["Admin"])){
    $styleAdmin = "";
}

//Submit Register

if (isset($_POST['RegistSubmit'])) {
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $name = $_POST["Name"];
    
    $sql = "INSERT INTO user (Username, Password,name, Status)
    VALUES ('$Username', '$Password','$name', 'Customer')";
    

    if (mysqli_query($conn, $sql)) {

    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
}
?>

<?php
//Submit login
include 'connection.php';


if(isset($_POST['LoginSubmit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    
    $sql = "SELECT * FROM user WHERE Username = '$Username' AND Password = '$Password' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $currentUser = $row["ID"];
    echo $currentUser;
    if ($result->num_rows > 0) {
        if($row["Status"] == "Admin"){
            $_SESSION["UserID"] = $row["ID"];
            $_SESSION["Admin"] = $row["Status"];
            $_SESSION["Username"] = $row["Status"];
            
            header("location:update.php");
        }else{
            $_SESSION["UserID"] = $row["ID"];
            $_SESSION["Username"] = $row["Name"];
            header("location:shop.php?signinsuccess=1");
        }
        
     
        session_write_close();
        
    }else{
        // echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        
         echo '<script type="text/javascript">alert("Incorrect Username or Password!");</script>';
    }
    mysqli_close($conn);
}

if (isset($_GET["action"])) {
    if ($_GET["action"] == "delete") {
        $sql = "DELETE FROM cart WHERE User_id=" . $_SESSION["UserID"] . " AND Item_id=" . $_GET["id"];
        echo $sql;
        $conn->query($sql);
        header("location:cart.php?remove=1");

        if (!isset($_SESSION["UserID"])) {
            $cookie_data = stripslashes($_COOKIE['shopping_cart']);
            $cart_data = json_decode($cookie_data, true);
            foreach ($cart_data as $keys => $values) {
                if ($cart_data[$keys]['item_id'] == $_GET["id"]) {
                    unset($cart_data[$keys]);
                    $item_data = json_encode($cart_data);
                    setcookie("shopping_cart", $item_data, time() + (86400 * 30));
                    header("location:cart.php?remove=1");
                }
            }

        }

    }
    if ($_GET["action"] == "clear") {
        setcookie("shopping_cart", "", time() - 3600);
        header("location:cart.php?clearall=1");
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Cart</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/logo.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">




</head>

<body>
    <!-- Search Wrapper Area Start -->
    <div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type your keyword...">
                            <button type="submit"><img src="img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Wrapper Area End -->
    <!-- Sign up Modal -->
    <div class="modal fade" id="RegistModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">SIGN UP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" placeholder="Enter username"
                                    name="Username">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                    name="Password">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Name"
                                    name="Name">
                            </div>

                            <button type="submit" name="RegistSubmit" class="btn btn-warning">Register</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Login modal -->
    <div id="LoginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SIGN IN</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username"
                                name="Username">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                name="Password">
                        </div>

                        <button type="submit" name="LoginSubmit" class="btn btn-warning">LOG IN</button>

                    </form>
                </div>

            </div>

        </div>
    </div>


    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

     
        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Amado Nav -->
            <nav class="amado-nav">
                <ul>
                <li><a href="index.php">Home</a></li>
                    <li><a href="shop.php">Shop</a></li>
                    <li  class="active"><a href="cart.php">Cart</a></li>
                    <li><a href="checkout.php">Checkout</a></li>
                    <li <?php echo $styleAdmin;?>><a href="update.php">Update</a></li>
                </ul>
            </nav>
            <!-- Button Group -->
            <div class="amado-btn-group mt-30 mb-100">
           
            <p id="islogin"  <?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
                <a id="signupBtn" <?php echo $style;?> href="#" class="btn amado-btn mb-15" data-toggle="modal" data-target="#RegistModal">SIGN UP</a>
                <a id="signinBtn" <?php echo $style;?> href="#" class="btn amado-btn active" data-toggle="modal" data-target="#LoginModal">SIGN IN</a>
                <a id="signoutBtn" <?php echo $styleSignout;?> href="shop.php?signout=1" class="btn amado-btn active">SIGN OUT</a>
            </div>
             <!-- Cart Menu -->
             <div class="cart-fav-search mb-100">
                <a href="cart.php" class="cart-nav"><img src="img/core-img/cart.png" alt=""> Cart <span>(<?php 
                if(isset($_SESSION["UserID"])){
                    $sql = "SELECT * FROM cart WHERE User_id=".$_SESSION["UserID"];
                    $result = $conn->query($sql);
                    echo $result->num_rows;

                }else{
                    if(isset($_COOKIE["shopping_cart"])){
                        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
                        $cart_data = json_decode($cookie_data, true);
                        echo count($cart_data);
                    }else{
                        echo 0;
                    }
                    
                }
           
                
                ?>)</span></a>

            </div>
          
        </header>
        <!-- Header Area End -->

        <div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="cart-title mt-50">
                            <h2>Shopping Cart</h2>
                        </div>

                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <th>
                                        <tr>
                                            <?php
                                            $total =0;
if (isset($_SESSION["UserID"])) {
    $sql = "Select User_id,Item_id,count(*) as `qty` ,items.Item_name,items.Item_price ,items.Item_image
                                    from cart,items
                                    where Item_id = items.ID AND User_id =" . $_SESSION["UserID"] . "
                                    GROUP BY User_id,Item_id
                                    having count(*) > 0;";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {

            ?>
                                            <td class="cart_product_img">
                                                <a href="#"><img src="<?php echo $row["Item_image"]; ?>" alt=""></a>
                                            </td>
                                            <td class="cart_product_desc">
                                                <h5><?php echo $row["Item_name"]; ?></h5>
                                            </td>
                                            <td class="price">
                                                <span>฿ <?php echo number_format($row["Item_price"], 2); ?></span>
                                            </td>
                                            <td class="qty">

                                                <span><?php echo $row["qty"]; ?></span>

                                            </td>
                                            <td>
                                                <span>฿
                                                    <?php echo number_format($row["qty"] * $row["Item_price"], 2); ?></span>
                                            </td>
                                            <td><a href="cart.php?action=delete&id=<?php echo $row["Item_id"]; ?>"><span
                                                        class="text-danger">Remove</span></a></td>
                                        </tr>
                                        <?php
$total = $total + ($row["qty"] * $row["Item_price"]);
        }

    } else {
        echo "0result";
    }

} else {
    if (isset($_COOKIE["shopping_cart"])) {
        $total = 0;
        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
        $cart_data = json_decode($cookie_data, true);
        foreach ($cart_data as $keys => $values) {

            ?>
                                    <td class="cart_product_img">
                                        <a href="#"><img src="<?php echo $values["item_image"]; ?>" alt=""></a>
                                    </td>
                                    <td class="cart_product_desc">
                                        <h5><?php echo $values["item_name"]; ?></h5>
                                    </td>
                                    <td class="price">
                                        <span>฿ <?php echo number_format($values["item_price"], 2); ?></span>
                                    </td>
                                    <td class="qty">

                                        <span><?php echo $values["item_quantity"]; ?></span>

                                    </td>
                                    <td>
                                        <span>฿
                                            <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></span>
                                    </td>
                                    <td><a href="cart.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span
                                                class="text-danger">Remove</span></a></td>
                                    </tr>
                                    <?php

            $total = $total + ($values["item_quantity"] * $values["item_price"]);
        }
        $_SESSION["TotalPrice"] = $total;
    } else {
        echo "0result";
    }
}
?>

                                    </th>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                        <form action="checkout.php">
                            <h5>Cart Total</h5>
                            <ul class="summary-table">
                                <li><span>subtotal:</span> <span>฿<?php echo number_format($_SESSION["TotalPrice"], 2); ?></span></li>
                                <li><span>delivery:</span> <span>Free</span></li>

                                <li><span>total:</span> <span>฿<?php echo number_format($_SESSION["TotalPrice"], 2); ?></span></li>


                            </ul>
                            <div class="cart-btn">
                                <input type="submit" href="" name="checkout" class="btn amado-btn w-100" value="Checkout"/>
                            </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- ##### Main Content Wrapper End ##### -->



    <!-- ##### Footer Area Start ##### -->
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                            <a href="index.php"><img src="img/core-img/logo2.png" alt=""></a>
                        </div>
                        <!-- Copywrite Text -->
                        <p class="copywrite">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="fa fa-heart-o"
                                aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu">
                            <nav class="navbar navbar-expand-lg justify-content-end">
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#footerNavContent" aria-controls="footerNavContent"
                                    aria-expanded="false" aria-label="Toggle navigation"><i
                                        class="fa fa-bars"></i></button>
                                <div class="collapse navbar-collapse" id="footerNavContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="index.php">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="shop.php">Shop</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="product-details.php">Product</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="cart.php">Cart</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="checkout.php">Checkout</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>

</body>

</html>