<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Cosmetic</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
	
	<!-- Stylesheets -->
	
	<link href="plugin-frameworks/bootstrap.min.css" rel="stylesheet">
	<link href="plugin-frameworks/swiper.css" rel="stylesheet">
	
	<link href="fonts/ionicons.css" rel="stylesheet">
	
		
	<link href="common/styles.css" rel="stylesheet">
	
	
</head>
<?php
session_start();
    
    $style = "";
    $styleSignout = "style='display:none;'";
    $styleAdmin = "style='display:none;'";
    if(isset($_SESSION['UserID'])){
        $style = "style='display:none;'";
        $styleSignout = "";
    }
    if(isset($_COOKIE['shopping_cart'])){
        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
        $cart_data = json_decode($cookie_data, true);
    }
    $currentUser=null;
    $Select=null;
    $sortingSql=null;
    if(isset($_SESSION["Admin"])){
        $styleAdmin = "";
	}
	
    if (isset($_GET["signout"])){
		session_destroy(); 
		header("location:index.php?signoutsuccess=1");
	}
	 include('connection.php');

	 if(isset($_POST['LoginSubmit']))
     {
         $Username = $_POST['Username'];
         $Password = $_POST['Password'];
         
         $sql = "SELECT * FROM user WHERE Username = '$Username' AND Password = '$Password' ";
         $result = $conn->query($sql);
         $row = $result->fetch_assoc();
         $currentUser = $row["ID"];
         echo $currentUser;
         if ($result->num_rows > 0) {
             if($row["Status"] == "admin"){
                 $_SESSION["UserID"] = $row["ID"];
                 $_SESSION["Admin"] = $row["Status"];
                 $_SESSION["Username"] = $row["Status"];
                 
                 header("location:sd.php");
             }else{
                 $_SESSION["UserID"] = $row["ID"];
                 $_SESSION["Username"] = $row["Name"];
                 header("location:index.php?signinsuccess=1");
             }
             
          
             session_write_close();
             
         }else{
             // echo "Error: " . $sql . "<br>" . mysqli_error($conn);
             
              echo '<script type="text/javascript">alert("Incorrect Username or Password!");</script>';
         }
         mysqli_close($conn);
	 }
	 if(isset($_POST['RegistSubmit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $name = $_POST["Name"];
    
    $sql = "INSERT INTO user (Username, Password,name, Status)
    VALUES ('$Username', '$Password','$name', 'Customer')";
    

if (mysqli_query($conn, $sql)) {
    
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

mysqli_close($conn);
}
    ?>
<body>
<div class="container">
        <!-- Sign up Modal -->
        <div class="modal fade" id="RegistModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">SIGN UP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" placeholder="Enter username"
                                    name="Username">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                    name="Password">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Name"
                                    name="Name">
                            </div>

                            <button type="submit" name="RegistSubmit" class="btn btn-warning">Register</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Login modal -->
    <div id="LoginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SIGN IN</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username"
                                name="Username">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                name="Password">
                        </div>

                        <button type="submit" name="LoginSubmit" class="btn btn-warning">LOG IN</button>

                    </form>
                </div>

            </div>

        </div>
    </div>

	<header>
		<a class="logo"><img src="images/logo3.jpg" alt="Logo"></a>
		
		<div class="right-area" style="">
		
			<p class="text-white"<?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
			<button <?php echo $style;?> type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#LoginModal">Login</button>
			<button <?php echo $style;?> type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#RegistModal">Register</button>
			<a <?php echo $styleSignout;?> href="index.php?signout=1" type="button" class="btn btn-danger">Logout</a>
		</div><!-- right-area -->
		
		<a class="menu-nav-icon" data-menu="#main-menu" href="#"><i class="ion-navicon"></i></a>
		
		<ul class="main-menu" id="main-menu">
			<li><a href="index.html">Home</a></li>
			<li><a href="Product.php">Product</a></li>
			<li><a href="#">Purchase Order</a></li>
			<li><a href="#">Inventory</a></li>
			<li class="drop-down"><a href="#!">MM<i class="ion-arrow-down-b"></i></a>
				<ul class="drop-down-menu drop-down-inner">
					<li><a href="#"></a>1</li>
					<li><a href="#"></a>2</li>
				</ul>
			</li>
			<li class="drop-down"><a href="#!">PLM<i class="ion-arrow-down-b"></i></a>
				<ul class="drop-down-menu drop-down-inner">
					<li><a href="#"></a>1</li>
					<li><a href="#"></a>2</li>
				</ul>
			</li>
			<li class="drop-down"><a href="#!">SD<i class="ion-arrow-down-b"></i></a>
				<ul class="drop-down-menu drop-down-inner">
					<li><a href="#"></a>1</li>
					<li><a href="#"></a>2</li>
				</ul>
			</li>
			<li><a href="#">Contact</a></li>
		</ul>
		
		<div class="clearfix"></div>
	</header>
	
	<div class="slider-main h-800x h-sm-auto pos-relative pt-95 pb-25">
		<div class="img-bg bg-1 bg-layer-4"></div>
		<div class="container-fluid h-100 mt-xs-50">
		
			<div class="row h-100">
				<div class="col-md-1"></div>
				<div class="col-sm-12 col-md-5 h-100 h-sm-50">
					<div class="dplay-tbl">
						<div class="dplay-tbl-cell color-white mtb-30">
							<div class="mx-w-400x">
								<h5><b>substances that you put on your face or body that are intended to improve your appearance</b></h5>
								<h1 class="mt-20 mb-30"><b>COSMETIC</b></h1>
							</div><!-- mx-w-200x -->
						</div><!-- dplay-tbl-cell -->
					</div><!-- dplay-tbl -->
				</div><!-- col-sm-6 -->
				
				<div class="col-sm-12 col-md-6 h-sm-50 oflow-hidden swiper-area pos-relative">			
	
					<div class="abs-blr pos-sm-static">
						<div class="row pos-relative mt-50 all-scroll">
						
							<div class="swiper-scrollbar resp"></div>
							<div class="col-md-10">
								
								<h5 class="mb-50 color-white"><b>BEST SELLER</b></h5>
								
								<div class="swiper-container oflow-visible" data-slide-effect="slide" data-autoheight="false" 
									data-swiper-speed="500" data-swiper-margin="25" data-swiper-slides-per-view="2"
									data-swiper-breakpoints="true" data-scrollbar="true" data-swiper-loop="true"
									data-swpr-responsive="[1, 2, 1, 2]">
									
									
								
									<div class="swiper-wrapper">
										<!-- data-swiper-autoplay="1000"  --> 
										<div class="swiper-slide">
											<div class="bg-white">
												<img src="images/powder1.jpg" alt="">
												
												<div class="plr-25 ptb-15">
													<h5 class="color-ash"><b>POUDRE UNIVERSELLE COMPACTE</b></h5>
													
													<ul class="list-li-mr-10 color-lt-black">
														<li><i class="mr-5 font-12 ion-android-favorite-outline"></i>15</li>
														<li><i class="mr-5 font-12 ion-ios-chatbubble-outline"></i>105</li>
													</ul>
												</div><!-- hot-news -->
											</div><!-- hot-news -->
										</div><!-- swiper-slide -->
										
										<div class="swiper-slide">
											<div class="bg-white">
												<img src="images/lip1.jpg" alt="">
												
												<div class="plr-25 ptb-15">
													<h5 class="color-ash"><b>ROUGE ALLURE VELVET EXTREME</b></h5>
													
													<ul class="list-li-mr-10 color-lt-black">
														<li><i class="mr-5 font-12 ion-android-favorite-outline"></i>15</li>
														<li><i class="mr-5 font-12 ion-ios-chatbubble-outline"></i>105</li>
													</ul>
												</div><!-- hot-news -->
											</div><!-- hot-news -->
										</div><!-- swiper-slide -->
										
										<div class="swiper-slide">
											<div class="bg-white">
												<img src="images/mascara1.jpg" alt="">
												
												<div class="plr-25 ptb-15">
													<h5 class="color-ash"><b>LE VOLUME DE CHANEL WATERPROOF<b></h5>
													
													<ul class="list-li-mr-10 color-lt-black">
														<li><i class="mr-5 font-12 ion-android-favorite-outline"></i>15</li>
														<li><i class="mr-5 font-12 ion-ios-chatbubble-outline"></i>105</li>
													</ul>
												</div><!-- hot-news -->
											</div><!-- hot-news -->
										</div><!-- swiper-slide -->
										
										
										
												</div><!-- hot-news -->
											</div><!-- hot-news -->
										</div><!-- swiper-slide -->
										
									</div><!-- swiper-wrapper -->
								</div><!-- swiper-container -->
								
							</div><!-- col-sm-6 -->
						</div><!-- all-scroll -->
					</div><!-- row -->
				</div><!-- col-sm-6 -->
				
			</div><!-- row -->
		</div><!-- container -->
	</div><!-- slider-main -->
	
	
	
				
						
	
	
	<footer class="bg-191 color-ash pt-50 pb-20 text-left center-sm-text">
		
		<div class="container-fluid">
			<div class="row">
			
				<div class="col-lg-1"></div>
				
				<div class="col-md-4 col-lg-6 mb-30">
					<div class="card h-100">
						<div class="dplay-tbl">
							<div class="dplay-tbl-cell">
							
								<a href="#"><img style="width:100px;" src="images/logo3.jpg"></a>
								<p class="color-ash mt-25"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ion-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
								
							</div><!-- dplay-tbl-cell -->
						</div><!-- dplay-tbl -->
					</div><!-- card -->
				</div><!-- col-lg-4 col-md-6 -->
				
				<div class="col-md-4 col-lg-2 mb-30">
					<div class="card h-100">
						<div class="dplay-tbl">
							<div class="dplay-tbl-cell">
							
								<ul class="list-a-plr-10">
									<li><a href="#"><i class="ion-social-facebook"></i></a></li>
									<li><a href="#"><i class="ion-social-twitter"></i></a></li>
									<li><a href="#"><i class="ion-social-youtube"></i></a></li>
								</ul>
								
							</div><!-- dplay-tbl-cell -->
						</div><!-- dplay-tbl -->
					</div><!-- card -->
				</div><!-- col-lg-4 col-md-6 -->
				
				<div class="col-md-4 col-lg-2 mb-30 text-left">
					<div class="card h-100">
						<div class="dplay-tbl">
							<div class="dplay-tbl-cell">
								<form class="form-block form-brdr-b mx-w-400x m-auto">
						
									<input class="color-white ptb-15 center-sm-text" type="text" placeholder="COSMETIC@GMAIL>COM">
		
								
								</form>
							</div><!-- dplay-tbl-cell -->
						</div><!-- dplay-tbl -->
					</div><!-- card -->
				</div><!-- col-lg-4 col-md-6 -->
				
			</div><!-- row -->
		</div><!-- container -->
	</footer>
	
	<!-- SCIPTS -->
	
	<script src="plugin-frameworks/jquery-3.2.1.min.js"></script>
	
	<script src="plugin-frameworks/bootstrap.min.js"></script>
	
	<script src="plugin-frameworks/swiper.js"></script>
	
	
	<script src="common/scripts.js"></script>
	
</body>
</html>