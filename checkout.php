<?php
include 'connection.php';
session_start();

$styleAdmin = "style='display:none;'";
$style = "";
$styleSignout = "style='display:none;'";
if (isset($_SESSION['UserID'])) {
    $style = "style='display:none;'";
    $styleSignout = "";

    $sql = "select sum(Item_price) as `sum`
                                from cart,items
                                where User_id =" . $_SESSION["UserID"] . " AND cart.Item_id = items.ID";

    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $_SESSION["TotalPrice"] = $row["sum"];
        session_write_close();
    }
}
if(isset($_SESSION["Admin"])){
    $styleAdmin = "";
}


if(isset($_POST['checkout'])){
    
    $name= $_POST["name"];
    $User_id = $_SESSION["UserID"];
    $Price = $_SESSION["TotalPrice"];
    $Phone = $_POST["phone_number"];
    $Email = $_POST["Email"];
    $Address = $_POST["street_address"]." ".$_POST["Town"]." ".$_POST["city"]." " .$_POST["country"]." ".$_POST["zipCode"];
    // $sql = "INSERT INTO orders (User_id,Firstname,Lastname,Order_price,Order_address,Phone_number) VALUE('$User_id','$firstname','$lastname',$Price,'$Address','$Phone')";
   $sql="INSERT INTO `orders` (`User_id`, `Name`,`Order_price`,`Order_address`,`Phone_number`,`Email`) VALUES ('$User_id','$name',$Price,'$Address','$Phone','$Email')";
    if (mysqli_query($conn, $sql)) {
        unset ($_SESSION["TotalPrice"]);
        $sql="DELETE FROM cart WHERE User_id=".$_SESSION["UserID"];
        mysqli_query($conn,$sql);
        echo '<script type="text/javascript">alert("Purchase successfully");</script>';
        header("location:index.php");
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    
    }
   
    
   
}



//Submit Register
if (isset($_POST['RegistSubmit'])) {
    $Username = $_POST['Username'];
    $name = $_POST["Name"];
    
$sql = "INSERT INTO user (Username, Password,name, Status)
VALUES ('$Username', '$Password','$name', 'Customer')";


    if (mysqli_query($conn, $sql)) {

    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);
}
?>

<?php
//Submit login
include 'connection.php';

if(isset($_POST['LoginSubmit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    
    $sql = "SELECT * FROM user WHERE Username = '$Username' AND Password = '$Password' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $currentUser = $row["ID"];
    echo $currentUser;
    if ($result->num_rows > 0) {
        if($row["Status"] == "Admin"){
            $_SESSION["UserID"] = $row["ID"];
            $_SESSION["Admin"] = $row["Status"];
            $_SESSION["Username"] = $row["Status"];
            
            header("location:update.php");
        }else{
            $_SESSION["UserID"] = $row["ID"];
            $_SESSION["Username"] = $row["Name"];
            header("location:shop.php?signinsuccess=1");
        }
        
     
        session_write_close();
        
    }else{
        // echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        
         echo '<script type="text/javascript">alert("Incorrect Username or Password!");</script>';
    }
    mysqli_close($conn);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags must come first in the head; any other head content must come after these tags -->

    <!-- Title  -->
    <title>Checkout</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/logo.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
   
<!-- Sign up Modal -->
<div class="modal fade" id="RegistModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">SIGN UP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" placeholder="Enter username"
                                    name="Username">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                    name="Password">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Name"
                                    name="Name">
                            </div>

                            <button type="submit" name="RegistSubmit" class="btn btn-warning">Register</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Login modal -->
    <div id="LoginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SIGN IN</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username"
                                name="Username">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                name="Password">
                        </div>

                        <button type="submit" name="LoginSubmit" class="btn btn-warning">LOG IN</button>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

      
        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Amado Nav -->
            <nav class="amado-nav">
                <ul>
                <li ><a href="index.php">Home</a></li>
                    <li><a href="shop.php">Shop</a></li>
                    <li ><a href="cart.php">Cart</a></li>
                    <li class="active"><a href="checkout.php">Checkout</a></li>
                    <li <?php echo $styleAdmin;?>><a href="update.php">Update</a></li>
                </ul>
            </nav>
            <!-- Button Group -->
            <div class="amado-btn-group mt-30 mb-100">
            <p id="islogin"  <?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
                <a id="signupBtn" <?php echo $style;?> href="#" class="btn amado-btn mb-15" data-toggle="modal" data-target="#RegistModal">SIGN UP</a>
                <a id="signinBtn" <?php echo $style;?> href="#" class="btn amado-btn active" data-toggle="modal" data-target="#LoginModal">SIGN IN</a>
                <a id="signoutBtn" <?php echo $styleSignout;?> href="shop.php?signout=1" class="btn amado-btn active">SIGN OUT</a>
            </div>
             <!-- Cart Menu -->
             <div class="cart-fav-search mb-100">
                <a href="cart.php" class="cart-nav"><img src="img/core-img/cart.png" alt=""> Cart <span>(<?php 
                if(isset($_SESSION["UserID"])){
                    $sql = "SELECT * FROM cart WHERE User_id=".$_SESSION["UserID"];
                    $result = $conn->query($sql);
                    echo $result->num_rows;

                }else{
                    if(isset($_COOKIE["shopping_cart"])){
                        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
                        $cart_data = json_decode($cookie_data, true);
                        echo count($cart_data);
                    }else{
                        echo 0;
                    }
                    
                }
           
                
                ?>)</span></a>

            </div>
          
        </header>
        <!-- Header Area End -->
            <!-- Social Button -->
            
        </header>
        <!-- Header Area End -->

        <div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-title">
                                <h2>Checkout</h2>
                            </div>

                            <form method="post">
                                <div class="row">
                                   
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control" id="name" name="name" value="" placeholder="Name" required>
                                    </div>
                                   
                                    <div class="col-12 mb-3">
                                        <input type="email" class="form-control" id="email" name="Email" placeholder="Email" value="">
                                    </div>
                                   
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control mb-3" id="street_address" name="street_address" placeholder="Address" value="">
                                    </div>
                                    <div class="col-12 mb-3">
                                        <input type="text" class="form-control" id="city" name="Town" placeholder="Town" value="">
                                    </div>
                                    <div class="col-12 mb-3">
                                        
                                        
                                        <input type="text" class="form-control" id="country" name="country" placeholder="Country" value="">
                                   
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <input type="text" class="form-control" id="zipCode" name="zipCode" placeholder="Zip Code" value="">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone No" value="">
                                    </div>
                            

                                    
                                </div>
                            
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                            <h5>Cart Total</h5>
                            <ul class="summary-table">
                                <li><span>subtotal:</span> <span>฿<?php echo number_format($_SESSION["TotalPrice"], 2); ?></span></li>
                                <li><span>delivery:</span> <span>Free</span></li>
                                <li><span>total:</span> <span>฿<?php echo number_format($_SESSION["TotalPrice"], 2); ?></span></li>
                            </ul>
                            <input type="hidden" name="TotalPrice" value="<?php echo $_SESSION["TotalPrice"]?>"/>
                           

                            <div class="cart-btn">
                                <input type="submit" href="" name="checkout" class="btn amado-btn w-100" value="Checkout"/>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Main Content Wrapper End ##### -->



    <!-- ##### Footer Area Start ##### -->
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                            <a href="index.php"><img src="img/core-img/logo2.png" alt=""></a>
                        </div>
                        <!-- Copywrite Text -->
                        <p class="copywrite"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu">
                            <nav class="navbar navbar-expand-lg justify-content-end">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#footerNavContent" aria-controls="footerNavContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                                <div class="collapse navbar-collapse" id="footerNavContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="index.php">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="shop.php">Shop</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="product-details.php">Product</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="cart.php">Cart</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="checkout.php">Checkout</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>

</body>

</html>