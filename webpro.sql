-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 21, 2019 at 01:12 AM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `webprodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `User_id`, `Item_id`) VALUES
(31, 14, 6),
(33, 14, 1),
(34, 14, 2),
(35, 14, 5),
(36, 14, 6),
(37, 14, 1),
(38, 14, 1),
(39, 14, 9);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `ID` int(11) NOT NULL,
  `Item_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Item_Brand` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Item_price` double(10,2) DEFAULT NULL,
  `Item_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Item_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Item_des` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`ID`, `Item_name`, `Item_Brand`, `Item_price`, `Item_type`, `Item_image`, `Item_des`) VALUES
(1, 'NIKE Mercurial Vapor 12 Elite FG ', 'NIKE', 8500.00, 'boot', 'https://thumbor-live.supersports.co.th/jabp0m3aZffk8nl2xIy6Zvyx864=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/nike-2317-46773-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(2, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 2170.00, 'boot', 'https://thumbor-live.supersports.co.th/hDwciv2ql9KUbaAJyF24B1tjKQQ=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/nike-9944-75503-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(5, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 123.00, 'boot', 'https://thumbor-live.supersports.co.th/jabp0m3aZffk8nl2xIy6Zvyx864=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/nike-2317-46773-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(6, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 456.00, 'boot', 'https://thumbor-live.supersports.co.th/hDwciv2ql9KUbaAJyF24B1tjKQQ=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/nike-9944-75503-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(7, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 789.00, 'boot', 'https://thumbor-live.supersports.co.th/hDwciv2ql9KUbaAJyF24B1tjKQQ=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/nike-9944-75503-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(8, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 7888.00, 'boot', 'https://thumbor-live.supersports.co.th/jabp0m3aZffk8nl2xIy6Zvyx864=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/nike-2317-46773-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(9, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 5555.00, 'shirt', 'https://thumbor-live.supersports.co.th/RSJw21aIpt1NxXTxxv5vMI1IVHU=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/new-balance-football-4332-44393-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(10, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 8888.00, 'shirt', 'https://thumbor-live.supersports.co.th/RSJw21aIpt1NxXTxxv5vMI1IVHU=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/new-balance-football-4332-44393-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(11, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 8888.00, 'shirt', 'https://thumbor-live.supersports.co.th/RSJw21aIpt1NxXTxxv5vMI1IVHU=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/new-balance-football-4332-44393-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(12, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 77777.00, 'shirt', 'https://thumbor-live.supersports.co.th/RSJw21aIpt1NxXTxxv5vMI1IVHU=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/new-balance-football-4332-44393-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน'),
(13, 'NIKE Mercurial Vapor 12 Academy (MG)', 'NIKE', 999.00, 'shirt', 'https://thumbor-live.supersports.co.th/RSJw21aIpt1NxXTxxv5vMI1IVHU=/fit-in/762x1100/filters:quality(90):fill(ffffff)/http://static-catalog.supersports.co.th/p/new-balance-football-4332-44393-1.jpg', 'สีเทา-เหลือง\r\n\r\n    อัปเปอร์วัสดุสังเคราะห์พรีเมี่ยม มอบสัมผัสที่แม่นยำและการควบคุมที่ความเร็วสูง\r\n    เทคโนโลยี ACC เคลือบผิวทำให้ผิวหน้าสัมผัสสามารถควบคุมลูกฟุตบอลได้ดีทั้งในสภาพเปียกและแห้ง\r\n    โครงสร้างแบบ 360 องศา โอบรับเท้าของคุณได้กระชับพอดีเหมือน');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `Order_id` int(11) NOT NULL,
  `User_id` int(100) DEFAULT NULL,
  `Order_price` double(11,2) DEFAULT NULL,
  `Order_address` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Phone_number` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Email` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`Order_id`, `User_id`, `Order_price`, `Order_address`, `Name`, `Phone_number`, `Email`) VALUES
(16, 12, 10670.00, '167 moo4 silalang pua nan  thailand 55120', 'Subtawee Hanyut', NULL, NULL),
(17, 12, 10670.00, '167 moo4 silalang pua nan  Thailand 55120', 'Subtawee', '0843786402', ''),
(18, 12, 10670.00, '167 moo4 silalang pua nan  thailand 55120', 'subtawee', '0843786402', 'subtawee.h@gmail.com'),
(19, 12, 10670.00, '167 moo4 silalang pua nan  thailand 55120', 'Subtawee', '0843786402', 'subtawee.h@gmail.com'),
(20, 12, 10670.00, '    ', 's', '', ''),
(21, 12, 10670.00, '167 moo4 silalang pua sdf  thailand 55120', 'Subtawee', '0843786402', 'subtawee.h@gmail.com'),
(22, 12, 10670.00, '    ', 's', '', ''),
(23, 12, 10670.00, '    ', 'xxx', '', ''),
(24, 12, 10670.00, '    ', 's', '', ''),
(25, 12, 10670.00, '    ', 'ddd', '', ''),
(26, 12, 8500.00, '    ', 'Subtawee', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(100) NOT NULL,
  `Username` varchar(100) COLLATE utf8_bin NOT NULL,
  `Password` varchar(100) COLLATE utf8_bin NOT NULL,
  `Status` varchar(30) COLLATE utf8_bin NOT NULL,
  `Name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Username`, `Password`, `Status`, `Name`, `LastUpdate`) VALUES
(12, 'admin', '1234', 'Customer', 'Subtawee', NULL),
(14, 'admin', '456', 'Customer', 'Hanyut', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `User_id_idx` (`id`,`User_id`),
  ADD KEY `User_id_idx1` (`User_id`),
  ADD KEY `Item_id_idx` (`Item_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Order_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `Order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `Item_id` FOREIGN KEY (`Item_id`) REFERENCES `items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `User_id` FOREIGN KEY (`User_id`) REFERENCES `user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
