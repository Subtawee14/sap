-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2019 at 06:02 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cosmetic`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Item_id` int(11) NOT NULL,
  `Qty` int(11) DEFAULT NULL,
  `Inquiry_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `User_id`, `Item_id`, `Qty`, `Inquiry_id`) VALUES
(1, 14, 14, 123, 1),
(2, 14, 15, 56, 1),
(3, 14, 16, 89, 1),
(4, 14, 17, 789, 1),
(5, 14, 18, 89, 1),
(6, 12, 14, 77, 2),
(7, 12, 15, 88, 2),
(8, 12, 16, 99, 2),
(9, 12, 17, 99, 2),
(10, 12, 18, 99, 2),
(11, 12, 14, 2, 3),
(12, 12, 15, 0, 3),
(13, 12, 16, 0, 3),
(14, 12, 17, 0, 3),
(15, 12, 18, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `com_id` int(11) NOT NULL,
  `com_name` varchar(255) NOT NULL,
  `com_add` varchar(255) NOT NULL,
  `com_tel` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`com_id`, `com_name`, `com_add`, `com_tel`) VALUES
(1, 'Winter is coming LTD.', 'A318 Gallery Hills,Suthep,Mueang,Chiang Mai,50200', '0835694041');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

CREATE TABLE `inquiry` (
  `Id` int(11) NOT NULL,
  `Inquiry_id` int(11) NOT NULL DEFAULT 0,
  `User_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inquiry`
--

INSERT INTO `inquiry` (`Id`, `Inquiry_id`, `User_id`, `date`, `status`) VALUES
(18, 1, 14, '2019-11-13 21:57:00', 'Approved'),
(19, 2, 12, '2019-11-13 21:57:12', 'Approved'),
(20, 3, 12, '2019-11-13 22:04:15', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry_detail`
--

CREATE TABLE `inquiry_detail` (
  `indetail_id` int(11) NOT NULL,
  `Inquiry_id` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inv_id` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`inv_id`, `ID`, `qty`) VALUES
(25, 14, 500),
(26, 18, 900),
(27, 17, 900),
(28, 16, 1200),
(29, 15, 500);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `invoice_date` datetime NOT NULL,
  `sale_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `invoice_date`, `sale_id`, `com_id`) VALUES
(6, '2019-11-13 22:00:08', 2, 1),
(7, '2019-11-13 22:00:08', 1, 1),
(8, '2019-11-14 00:02:00', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `ID` int(11) NOT NULL,
  `Item_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Item_Brand` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Item_price` double(10,2) DEFAULT NULL,
  `Item_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Item_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `Item_des` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`ID`, `Item_name`, `Item_Brand`, `Item_price`, `Item_type`, `Item_image`, `Item_des`) VALUES
(14, 'rouge allure velvet extreme', 'chanal', 1500.00, 'lipstick', 'img/Lip-1.JPG', 'CHANEL pushes the boundaries of matte with ROUGE ALLURE VELVET EXTR?ME. An elegant and sophisticated lipstick with an extremely matte second-skin finish.\r\nIts innovative texture offers an extra-matte makeup result with intense and long-wear colour '),
(15, 'POUDRE UNIVERSELLE COMPACTE\r\n', 'chanal', 2500.00, 'powder', 'img/Powder-1.JPG', 'A powder that mattifies the complexion and refines the skin texture. An ultra-fine, evanescent compact texture. A practical, travel-friendly case for easy touch-ups throughout the day. '),
(16, 'les 4 ombres', 'chanal', 1800.00, 'eye shadow', 'img/Eye-1.JPG', 'A palette of four eyeshadows with a baked powder texture. An array of personalised colours. A harmony of timeless shades with a matte, satiny or metallic finish, offering endless variations and contrasts.'),
(17, 'le volume de chanel waterproof', 'chanal', 1200.00, 'mascara', 'img/Mascara-1.JPG', 'A deeply intense mascara that emphasises lashes and dresses their fringe with a kohl-line illusion.'),
(18, 'les beiges', 'chanal', 2500.00, 'foundation', 'img/Foundation-1.JPG', 'On the cutting edge of microfluidic technologies in the cosmetics field, CHANEL creates EAU DE TEINT, the first water-fresh tint by CHANEL.');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `mat_id` int(11) NOT NULL,
  `mat_name` varchar(255) DEFAULT NULL,
  `mat_price` double(10,2) DEFAULT NULL,
  `mat_image` varchar(255) DEFAULT NULL,
  `vendor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`mat_id`, `mat_name`, `mat_price`, `mat_image`, `vendor_id`) VALUES
(1, 'Lip Tube', 300.00, 'img/lip-tube.jpg', 0),
(2, 'Lip lid', 40.00, 'img/lip-lid.jpg', 0),
(3, 'Lip Texture', 200.00, 'img/lip-texture.jpg', 0),
(4, 'Eye Case', 400.00, 'img/eye-case.jpg', 0),
(5, 'Eye Brush', 100.00, 'img/eye-brush.jpg', 0),
(6, 'Eyeshadow', 400.00, 'img/eyeshadow.jpg', 0),
(7, 'Foundation Texture', 400.00, 'img/foundation-texture.jpg', 0),
(8, 'Glass Tube', 300.00, 'img/glass-tube.jpg', 0),
(9, 'Lid', 70.00, 'img/tube-lid.jpg', 0),
(10, 'Powder Case', 300.00, 'img/powder-case.jpg', 0),
(11, 'Press Powder', 300.00, 'img/press-powder.jpg', 0),
(12, 'Puff', 70.00, 'img/puff.jpg', 0),
(13, 'Mascara Tube', 200.00, 'img/mascara-tube.jpg', 0),
(14, 'Mascara Brush', 200.00, 'img/mascara-brush.jpg', 0),
(15, 'Masscara', 100.00, 'img/mascara.jpg', 0),
(20, 'Subbb', 120.15, 'img/sub.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `Order_id` int(11) NOT NULL,
  `User_id` int(100) DEFAULT NULL,
  `Order_price` double(11,2) DEFAULT NULL,
  `Order_address` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Phone_number` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `Email` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `product_detail`
--

CREATE TABLE `product_detail` (
  `detail_id` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `mat_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_detail`
--

INSERT INTO `product_detail` (`detail_id`, `ID`, `mat_id`, `qty`) VALUES
(1, 16, 4, 1),
(2, 16, 5, 1),
(3, 16, 6, 1),
(4, 18, 7, 1),
(5, 18, 8, 1),
(6, 18, 9, 1),
(7, 17, 13, 1),
(8, 17, 14, 1),
(9, 17, 15, 1),
(10, 15, 10, 1),
(11, 15, 11, 1),
(12, 15, 12, 1),
(14, 14, 1, 1),
(15, 14, 2, 1),
(16, 14, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `quo_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `quo_exp` datetime NOT NULL,
  `com_id` int(11) NOT NULL,
  `inquiry_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`quo_id`, `date`, `quo_exp`, `com_id`, `inquiry_id`) VALUES
(36, '2019-11-13 21:55:58', '2019-11-28 21:55:58', 1, 2),
(37, '2019-11-13 21:57:17', '2019-11-28 21:57:17', 1, 1),
(38, '2019-11-13 21:57:17', '2019-11-28 21:57:17', 1, 2),
(39, '2019-11-13 22:04:24', '2019-11-28 22:04:24', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `saleorder`
--

CREATE TABLE `saleorder` (
  `sale_id` int(11) NOT NULL DEFAULT 0,
  `sale_date` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `req_dev_date` datetime NOT NULL,
  `quo_id` int(11) NOT NULL,
  `com_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `Inquiry_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `saleorder`
--

INSERT INTO `saleorder` (`sale_id`, `sale_date`, `user_id`, `req_dev_date`, `quo_id`, `com_id`, `status`, `Inquiry_id`) VALUES
(1, '2019-11-13', 12, '2019-11-18 00:00:00', 36, 1, 'Approved', 2),
(2, '2019-11-13', 14, '2019-11-12 00:00:00', 37, 1, 'Approved', 1),
(3, '2019-11-14', 12, '2019-11-19 00:00:00', 39, 1, 'Approved', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Username` varchar(100) COLLATE utf8_bin NOT NULL,
  `Password` varchar(100) COLLATE utf8_bin NOT NULL,
  `Status` varchar(30) COLLATE utf8_bin NOT NULL,
  `Name` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `LastName` varchar(255) COLLATE utf8_bin NOT NULL,
  `address` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `Username`, `Password`, `Status`, `Name`, `LastName`, `address`, `tel`) VALUES
(12, 'admin', '1234', 'Customer', 'Subtawee', 'Hanyut', 'A318 cafe', '0811111111'),
(14, 'admin', '456', 'Member', 'Chotika', 'Nalampang', 'A318 cafe', '0830255747'),
(15, 'june', '1234', 'admin', 'Janissata', 'Teepakakorn', '418/3', '0985653659');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendor_id` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL,
  `vendor_add` varchar(255) NOT NULL,
  `vendor_tel` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendor_id`, `vendor_name`, `vendor_add`, `vendor_tel`) VALUES
(1, 'J-leng Kornwan', '123/12 moo3,pas district,lamphun 74000', '0975678910'),
(2, 'GopGap LTD.', '919 moo10,south nakhon District,Nakhonsawan 60200', '0902039200'),
(4, 'subtawee', '318 cafe', '0811111111'),
(5, 'asd', 'd', 'asd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `User_id_idx` (`id`,`User_id`),
  ADD KEY `User_id_idx1` (`User_id`),
  ADD KEY `Item_id_idx` (`Item_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_link` (`User_id`);

--
-- Indexes for table `inquiry_detail`
--
ALTER TABLE `inquiry_detail`
  ADD PRIMARY KEY (`indetail_id`),
  ADD KEY `Inquiry_id` (`Inquiry_id`,`ID`),
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `ID` (`ID`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`),
  ADD KEY `com_id` (`com_id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`mat_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Order_id`);

--
-- Indexes for table `product_detail`
--
ALTER TABLE `product_detail`
  ADD PRIMARY KEY (`detail_id`),
  ADD KEY `ID` (`ID`,`mat_id`),
  ADD KEY `mat_id` (`mat_id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`quo_id`),
  ADD KEY `ID` (`com_id`);

--
-- Indexes for table `saleorder`
--
ALTER TABLE `saleorder`
  ADD PRIMARY KEY (`sale_id`),
  ADD KEY `user_id` (`user_id`,`quo_id`,`com_id`),
  ADD KEY `com_id` (`com_id`),
  ADD KEY `quo_id` (`quo_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inquiry`
--
ALTER TABLE `inquiry`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `inquiry_detail`
--
ALTER TABLE `inquiry_detail`
  MODIFY `indetail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `mat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `Order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_detail`
--
ALTER TABLE `product_detail`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `quo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `Item_id` FOREIGN KEY (`Item_id`) REFERENCES `items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD CONSTRAINT `User_link` FOREIGN KEY (`User_id`) REFERENCES `user` (`ID`);

--
-- Constraints for table `inquiry_detail`
--
ALTER TABLE `inquiry_detail`
  ADD CONSTRAINT `inquiry_detail_ibfk_1` FOREIGN KEY (`Inquiry_id`) REFERENCES `inquiry` (`Id`),
  ADD CONSTRAINT `inquiry_detail_ibfk_2` FOREIGN KEY (`ID`) REFERENCES `items` (`ID`);

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `items` (`ID`);

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`),
  ADD CONSTRAINT `invoice_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `saleorder` (`sale_id`);

--
-- Constraints for table `product_detail`
--
ALTER TABLE `product_detail`
  ADD CONSTRAINT `product_detail_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `items` (`ID`),
  ADD CONSTRAINT `product_detail_ibfk_2` FOREIGN KEY (`mat_id`) REFERENCES `material` (`mat_id`);

--
-- Constraints for table `quotation`
--
ALTER TABLE `quotation`
  ADD CONSTRAINT `quotation_ibfk_1` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`);

--
-- Constraints for table `saleorder`
--
ALTER TABLE `saleorder`
  ADD CONSTRAINT `saleorder_ibfk_1` FOREIGN KEY (`com_id`) REFERENCES `company` (`com_id`),
  ADD CONSTRAINT `saleorder_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`ID`),
  ADD CONSTRAINT `saleorder_ibfk_4` FOREIGN KEY (`quo_id`) REFERENCES `quotation` (`quo_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
