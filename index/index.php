<?php
include('connection.php');
session_start();
if(isset($_COOKIE['shopping_cart'])){
    $cookie_data = stripslashes($_COOKIE['shopping_cart']);
    $cart_data = json_decode($cookie_data, true);
}

$style = "";

    $styleSignout = "style='display:none;'";
    $styleAdmin = "style='display:none;'";
    if(isset($_SESSION['UserID'])){
        $style = "style='display:none;'";
        $styleSignout = "";
    }
    //Submit Register
if(isset($_SESSION["Admin"])){
    $styleAdmin = "";
}

if(isset($_POST['RegistSubmit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $name = $_POST["Name"];
    
$sql = "INSERT INTO user (Username, Password,name, Status)
VALUES ('$Username', '$Password','$name', 'Customer')";

if (mysqli_query($conn, $sql)) {
    
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

mysqli_close($conn);
}
?>

<?php
//Submit login
     include('connection.php');

if(isset($_POST['LoginSubmit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    
    $sql = "SELECT * FROM user WHERE Username = '$Username' AND Password = '$Password' ";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $currentUser = $row["ID"];
    echo $currentUser;
    if ($result->num_rows > 0) {
        if($row["Status"] == "Admin"){
            $_SESSION["UserID"] = $row["ID"];
            $_SESSION["Admin"] = $row["Status"];
            $_SESSION["Username"] = $row["Status"];
            
            header("location:update.php");
        }else{
            $_SESSION["UserID"] = $row["ID"];
            $_SESSION["Username"] = $row["Name"];
            header("location:shop.php?signinsuccess=1");
        }
        
     
        session_write_close();
        
    }else{
        // echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        
         echo '<script type="text/javascript">alert("Incorrect Username or Password!");</script>';
    }
    mysqli_close($conn);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Home</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/logo.ico">
   

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Search Wrapper Area Start -->
    <div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type your keyword...">
                            <button type="submit"><img src="img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Wrapper Area End -->
    <!-- Sign up Modal -->
    <div class="modal fade" id="RegistModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">SIGN UP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" placeholder="Enter username"
                                    name="Username">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                    name="Password">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Name"
                                    name="Name">
                            </div>

                            <button type="submit" name="RegistSubmit" class="btn btn-warning">Register</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Login modal -->
    <div id="LoginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SIGN IN</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username"
                                name="Username">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                name="Password">
                        </div>

                        <button type="submit" name="LoginSubmit" class="btn btn-warning">LOG IN</button>

                    </form>
                </div>

            </div>

        </div>
    </div>


    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Amado Nav -->
            <nav class="amado-nav">
                <ul>
                <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="shop.php">Shop</a></li>
                    <li ><a href="cart.php">Cart</a></li>
                    <li><a href="checkout.php">Checkout</a></li>
                    <li <?php echo $styleAdmin;?>><a href="update.php">Update</a></li>
                </ul>
            </nav>
            <!-- Button Group -->
            <div class="amado-btn-group mt-30 mb-100">
            <p id="islogin"  <?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
                <a id="signupBtn" <?php echo $style;?> href="#" class="btn amado-btn mb-15" data-toggle="modal" data-target="#RegistModal">SIGN UP</a>
                <a id="signinBtn" <?php echo $style;?> href="#" class="btn amado-btn active" data-toggle="modal" data-target="#LoginModal">SIGN IN</a>
                <a id="signoutBtn" <?php echo $styleSignout;?> href="shop.php?signout=1" class="btn amado-btn active">SIGN OUT</a>
            </div>
             <!-- Cart Menu -->
             <div class="cart-fav-search mb-100">
                <a href="cart.php" class="cart-nav"><img src="img/core-img/cart.png" alt=""> Cart <span>(<?php 
                if(isset($_SESSION["UserID"])){
                    $sql = "SELECT * FROM cart WHERE User_id=".$_SESSION["UserID"];
                    $result = $conn->query($sql);
                    echo $result->num_rows;

                }else{
                    if(isset($_COOKIE["shopping_cart"])){
                        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
                        $cart_data = json_decode($cookie_data, true);
                        echo count($cart_data);
                    }else{
                        echo 0;
                    }
                    
                }
           
                
                ?>)</span></a>

            </div>
          
        </header>
        <!-- Header Area End -->

        <!-- Product Catagories Area Start -->
        <div class="products-catagories-area clearfix">
            <div class="amado-pro-catagory clearfix">
                <?php

$sql = "SELECT Item_name, Item_price, Item_image FROM items";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["Item_name"]. " - Name: " . $row["Item_price"]. " " . $row["Item_image"]. "<br>";
       echo '
       <!-- Single Catagory -->
       <div class="single-products-catagory clearfix" >
           <a href="shop.php">
               <img src="'.$row["Item_image"].'" alt="">
               <!-- Hover Content -->
               
           </a>
       </div>';
                    

    }
    
    
} else {
    echo "0 results";
}
$conn->close();
?>
<!--                  
               <div class="hover-content">
                   <div class="line"></div>
                   <p>'.$row["Item_price"].'</p>
                   <h4>'.$row["Item_name"].'</h4>
               </div> -->
            
            
            </div>
        </div>
        <!-- Product Catagories Area End -->
    </div>
 
  

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>

</body>

</html>