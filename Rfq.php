
<?php
 include('connection.php');
 session_start();
    $style = "";
    $styleSignout = "style='display:none;'";
    $styleAdmin = "style='display:none;'";
    if(isset($_SESSION['UserID'])){
        $style = "style='display:none;'";
        $styleSignout = "";
    }else{
        echo '<script type="text/javascript">'; 
            echo 'alert("Please Login");'; 
            echo 'window.location.href = "index.php";';
            echo '</script>';
       
    }
?>

<html>
<head>
	<title>Request</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Stylesheets -->
	
	<!-- <link href="plugin-frameworks/bootstrap.min.css" rel="stylesheet"> -->
	<link href="plugin-frameworks/swiper.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
	<link href="fonts/ionicons.css" rel="stylesheet">
	
		
	<link href="common/styles.css" rel="stylesheet">
	
	
</head>
<body>
<?php

if(isset($_POST['submitQt'])){
    // Auto ID
$startId = "OR";
$tablename= "inquiry";
$startnumber="10000";
$sqlAuto = "SELECT * from $tablename";
$result = $conn->query($sqlAuto);
if($result->num_rows == 0){
    $maxId = 1;
}else{
    $maxId = $result->num_rows;
    echo $maxId;
    $maxId = ($maxId+1);  
}
    
    $User_id = $_SESSION["UserID"];
   $Item_id= $_POST['Item_id'];
    $Qty = $_POST['Qty'];
    date_default_timezone_set("Asia/Bangkok");
    $date = date('Y-m-d H:i:s');
    $sql = 'INSERT INTO cart (User_id,Item_id,Qty,inquiry_id) VALUES';
    $Inquiry = "INSERT INTO inquiry (Inquiry_id,User_id,date,status) VALUE ($maxId,$User_id,'$date','Pending')";
//    $sql = "INSERT INTO cart (User_id,Item_id,Qty) 
//             VALUES ('$User_id','$Item_id[1]','$Qty[1]')";

    for ($i=0; $i<count($_POST['Item_id']); $i++) {
        // $sql .= '(\'' . $_POST['Item_id'][$i] . '\', \'' . $_POST['Qty'][$i] . '\')';
        $sql .= "($User_id,$Item_id[$i],$Qty[$i],$maxId)";
    if ($i<count($_POST['Item_id']) - 1) {
       $sql .= ',';
    }
  }
    if (mysqli_query($conn, $sql)) {
      mysqli_query($conn, $Inquiry);
      echo '<script type="text/javascript">'; 
      echo 'alert("Submit Inquiry Successfully");'; 
      echo 'window.location.href = "index.php";';
      echo '</script>';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    
    }

}  
   
    
   


    $style = "";
    $styleSignout = "style='display:none;'";
    $styleAdmin = "style='display:none;'";
    if(isset($_SESSION['UserID'])){
        $style = "style='display:none;'";
        $styleSignout = "";
    }
    if(isset($_COOKIE['shopping_cart'])){
        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
        $cart_data = json_decode($cookie_data, true);
    }
    $currentUser=null;
    $Select=null;
    $sortingSql=null;
    if(isset($_SESSION["Admin"])){
        $styleAdmin = "";
	}
	
    if (isset($_GET["signout"])){
		session_destroy(); 
		header("location:index.php?signoutsuccess=1");
    }
    
    ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    <p class="text-white"<?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
			<button <?php echo $style;?> type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#LoginModal">Login</button>
			<button <?php echo $style;?> type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#RegistModal">Register</button>
			<a <?php echo $styleSignout;?> href="Product.php?signout=1" type="button" class="btn btn-danger">Log out</a>
    </div>
  </div>
</nav>
<div class="container">
<table class="table mt-50">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Product Name</th>
      <th scope="col">Quantity</th>

    </tr>
  </thead>
  <tbody>
<form method="post">
  <?php

include('connection.php');


$sql= " SELECT * FROM items";


$result = $conn->query($sql);



if ($result->num_rows > 0) {

while($row = $result->fetch_assoc()) {
 

?>
    <tr>
        
            <th scope="row"><input class="inputUpdate form-control"  type="hidden" name="Item_id[]" value="<?php echo $row["ID"]; ?>" /><?php echo $row["ID"]; ?></th>
            <td><?php echo $row["Item_name"]; ?></td>
            <td><input class="inputUpdate form-control"  type="number" name="Qty[]" value="0" /></td>
        
    </tr>
    <?php
    }
   
} else {
    echo "0 results";
}

// function addTocart($userID,$itemID){
   
// }

$conn->close();
?>

  </tbody>
  
</table>
<button style="float:right;" type="submit" name="submitQt" class="btn btn-success">Submit</button>
</form>

</div>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>