<?php
 include('connection.php');
 session_start();
    $style = "";
    $styleSignout = "style='display:none;'";
    $styleAdmin = "style='display:none;'";
    if(isset($_SESSION['UserID'])){
        $style = "style='display:none;'";
        $styleSignout = "";
    }else{
        echo '<script type="text/javascript">'; 
            echo 'alert("Please Login");'; 
            echo 'window.location.href = "index.php";';
            echo '</script>';
       
    }
?>
<html>
<head>
	<title>Vendor</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	
	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Stylesheets -->
	
	<!-- <link href="plugin-frameworks/bootstrap.min.css" rel="stylesheet"> -->
	<link href="plugin-frameworks/swiper.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	
	<link href="fonts/ionicons.css" rel="stylesheet">
	
		
	<link href="common/styles.css" rel="stylesheet">
	
	
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Vendor</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
    <p class="text-white"<?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
			<button <?php echo $style;?> type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#LoginModal">Login</button>
			<button <?php echo $style;?> type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#RegistModal">Register</button>
			<a <?php echo $styleSignout;?> href="Product.php?signout=1" type="button" class="btn btn-danger">Log out</a>
    </div>
  </div>
</nav>
<div class="container">
<table class="table mt-50">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Vendor Name</th>
      <th scope="col">Vendor Address</th>
	  <th scope="col">Vendor Tel</th>
	  <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
<form method="post">
  <?php

include('connection.php');


$sql= " SELECT * FROM vendor";


$result = $conn->query($sql);



if ($result->num_rows > 0) {

while($row = $result->fetch_assoc()) {
 

?>
    <tr>
        <form>
            <th scope="row"><input class="inputUpdate form-control"  type="hidden" name="ID[]" value="
			<?php echo $row["vendor_id"]; ?>" />
			<?php echo $row["vendor_id"]; ?></th>
			<td><?php echo $row["vendor_name"]; ?></td>
			<td><?php echo $row["vendor_add"]; ?></td>
			<td><?php echo $row["vendor_tel"]; ?></td>
			<td><a href="delete_vendor.php?vendor_id=<?php echo $row['vendor_id'];?>">Delete</a></td>
        </form>
    </tr>
	
    <?php
    }
   
} else {
    echo "0 results";
}

// function addTocart($userID,$itemID){
   
// }

$conn->close();
?>

  </tbody>
  
</table>

</form>
<a href="edit_vendor.php" style="float:right;"  name="edit" class="btn btn-success">Edit</a>
<a href="insert_vendor.php" style="position:relative; right:-970px"  name="edit" class="btn btn-success">Insert</a>
</div>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>