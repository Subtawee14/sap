<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Title  -->
    <title>Shop</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/logo.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">
    <?php
    
    session_start();
    
    $style = "";
    $styleSignout = "style='display:none;'";
    $styleAdmin = "style='display:none;'";
    if(isset($_SESSION['UserID'])){
        $style = "style='display:none;'";
        $styleSignout = "";
    }
    if(isset($_COOKIE['shopping_cart'])){
        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
        $cart_data = json_decode($cookie_data, true);
    }
    $currentUser=null;
    $Select=null;
    $sortingSql=null;
    if(isset($_SESSION["Admin"])){
        $styleAdmin = "";
    }
    
    ?>

</head>

<?php

if (isset($_GET['Select'])) {
    $Select=$_GET["Select"];
}
if (isset($_GET["signout"])){
    session_destroy(); 
    header("location:shop.php?signoutsuccess=1");
}
 include('connection.php');

 

//Submit Register
    

if(isset($_POST['RegistSubmit']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $name = $_POST["Name"];
    
    $sql = "INSERT INTO user (Username, Password,name, Status)
    VALUES ('$Username', '$Password','$name', 'Customer')";
    

if (mysqli_query($conn, $sql)) {
    
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

mysqli_close($conn);
}
?>

<?php
//Submit login
     include('connection.php');


     if(isset($_POST['LoginSubmit']))
     {
         $Username = $_POST['Username'];
         $Password = $_POST['Password'];
         
         $sql = "SELECT * FROM user WHERE Username = '$Username' AND Password = '$Password' ";
         $result = $conn->query($sql);
         $row = $result->fetch_assoc();
         $currentUser = $row["ID"];
         echo $currentUser;
         if ($result->num_rows > 0) {
             if($row["Status"] == "Admin"){
                 $_SESSION["UserID"] = $row["ID"];
                 $_SESSION["Admin"] = $row["Status"];
                 $_SESSION["Username"] = $row["Status"];
                 
                 header("location:update.php");
             }else{
                 $_SESSION["UserID"] = $row["ID"];
                 $_SESSION["Username"] = $row["Name"];
                 header("location:Product.php?signinsuccess=1");
             }
             
          
             session_write_close();
             
         }else{
             // echo "Error: " . $sql . "<br>" . mysqli_error($conn);
             
              echo '<script type="text/javascript">alert("Incorrect Username or Password!");</script>';
         }
         mysqli_close($conn);
     }
?>
<?php
if(isset($_POST["add_to_cart"]))
{
    if(isset($_SESSION["UserID"])){
        $addtocart = "INSERT INTO cart(User_id,Item_id) VALUE(".$_SESSION["UserID"].",".$_POST["hidden_id"].")";
        $conn->query($addtocart);
    }else{
        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
        $cart_data = json_decode($cookie_data, true);
        $item_id_list = array_column($cart_data, 'item_id');
        if(in_array($_POST["hidden_id"], $item_id_list))
	{
		foreach($cart_data as $keys => $values)
		{
			if($cart_data[$keys]["item_id"] == $_POST["hidden_id"])
			{
				$cart_data[$keys]["item_quantity"] = $cart_data[$keys]["item_quantity"] + $_POST["quantity"];
			}
		}
	}
	else
	{
		$item_array = array(
            'user_id'           =>  $_SESSION["UserID"],
			'item_id'			=>	$_POST["hidden_id"],
			'item_name'			=>	$_POST["hidden_name"],
			'item_price'		=>	$_POST["hidden_price"],
            'item_quantity'		=>	$_POST["quantity"],
            'item_image'        =>  $_POST["hidden_image"]
		);
		$cart_data[] = $item_array;
	}

	
	$item_data = json_encode($cart_data);
	setcookie('shopping_cart', $item_data, time() + (86400 * 30));
    }
 
	header("location:shop.php?success=1");
}
?>
<?php

//Sorting
     include('connection.php');

if(isset($_GET['sorting']))
{
    $sorting_type = $_GET["sorting"];
    echo $sorting_type;
    if($sorting_type=="SortByName"){
        $sql = "SELECT * FROM items WHERE `Item_type` = '$Select' ORDER BY item_name";
    }else if($sorting_type=="PriceASC"){
        $sql = "SELECT * FROM items WHERE `Item_type` = '$Select' ORDER BY item_price ASC";
    }else{
        $sql = "SELECT * FROM items WHERE `Item_type` = '$Select' ORDER BY item_price DESC";
    }
    $sortingSql = $sql;
}
?>

<body>
    <div class="container">
        <!-- Sign up Modal -->
        <div class="modal fade" id="RegistModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">SIGN UP</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="username">Username:</label>
                                <input type="text" class="form-control" id="username" placeholder="Enter username"
                                    name="Username">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                    name="Password">
                            </div>
                            <div class="form-group">
                                <label for="pwd">Name:</label>
                                <input type="text" class="form-control" id="name" placeholder="Enter Name"
                                    name="Name">
                            </div>

                            <button type="submit" name="RegistSubmit" class="btn btn-warning">Register</button>

                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- Login modal -->
    <div id="LoginModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">SIGN IN</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter username"
                                name="Username">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Password:</label>
                            <input type="password" class="form-control" id="pwd" placeholder="Enter password"
                                name="Password">
                        </div>

                        <button type="submit" name="LoginSubmit" class="btn btn-warning">LOG IN</button>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <!-- Search Wrapper Area Start -->
    <div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type your keyword...">
                            <button type="submit"><img src="img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Wrapper Area End -->

    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

 
        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.php"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Amado Nav -->
            <nav class="amado-nav">
                <ul>
                <li ><a href="index.php">Home</a></li>
                    <li class="active"><a href="shop.php">Shop</a></li>
                    <li ><a href="cart.php">Cart</a></li>
                    <li><a href="checkout.php">Checkout</a></li>
                    <li <?php echo $styleAdmin;?>><a href="update.php">Update</a></li>
                </ul>
            </nav>
            <!-- Button Group -->
            <div class="amado-btn-group mt-30 mb-100">
            <p id="islogin"  <?php echo $styleSignout;?>><?php echo $_SESSION["Username"];?></p>
                <a id="signupBtn" <?php echo $style;?> href="#" class="btn amado-btn mb-15" data-toggle="modal" data-target="#RegistModal">SIGN UP</a>
                <a id="signinBtn" <?php echo $style;?> href="#" class="btn amado-btn active" data-toggle="modal" data-target="#LoginModal">SIGN IN</a>
                <a id="signoutBtn" <?php echo $styleSignout;?> href="shop.php?signout=1" class="btn amado-btn active">SIGN OUT</a>
            </div>
             <!-- Cart Menu -->
             <div class="cart-fav-search mb-100">
                <a href="cart.php" class="cart-nav"><img src="img/core-img/cart.png" alt=""> Cart <span>(<?php 
                if(isset($_SESSION["UserID"])){
                    $sql = "SELECT * FROM cart WHERE User_id=".$_SESSION["UserID"];
                    $result = $conn->query($sql);
                    echo $result->num_rows;

                }else{
                    if(isset($_COOKIE["shopping_cart"])){
                        $cookie_data = stripslashes($_COOKIE['shopping_cart']);
                        $cart_data = json_decode($cookie_data, true);
                        echo count($cart_data);
                    }else{
                        echo 0;
                    }
                    
                }
           
                
                ?>)</span></a>

            </div>
          
        </header>
        <!-- Header Area End -->

        <div class="shop_sidebar_area">

            <!-- ##### Single Widget ##### -->
            <div class="widget catagory mb-50">
                <!-- Widget Title -->
                <h6 class="widget-title mb-30">Catagories</h6>

                <!--  Catagories  -->
                <div class="catagories-menu">
                    <ul>
                        <?php 
                            $sql="SELECT DISTINCT Item_type FROM items";
                            $result = $conn->query($sql);
                        
                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    
                        ?>
                        <li ><a class="catagory" href="shop.php?Select=<?php echo $row['Item_type'];?>" ><?php echo $row['Item_type']?></a></li>
                        
                       <?php
                                }
                            }
                       ?>

                    </ul>
                </div>
            </div>
        </div>

        <div class="amado_product_area section-padding-100">
            <div class="container-fluid">

                
                <?php

if($Select!=null){
   $sql= " SELECT * FROM items WHERE `Item_type` = '$Select'";
}else{
    $sql= " SELECT * FROM items";
}

    $result = $conn->query($sql);



if ($result->num_rows > 0) {
    // output data of each row
    echo '<div class="row">';
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["Item_name"]. " - Name: " . $row["Item_price"]. " " . $row["Item_image"]. "<br>";
       ?>
                  
                    
                <div id="box" class="col-12 col-sm-3 col-md-12 col-xl-3">
				<form method="post">
					<div align="center">
                        <div class="crop ">
						    <img src="<?php echo $row["Item_image"]; ?>" /><br />
                        </div>
                        <div class="product-name">
                            <h6 ><?php echo $row["Item_name"]; ?></h4>
                        </div>
                       
						<h4 class="product-price" style="color:green;">฿ <?php echo $row["Item_price"]; ?></h4>

						<input type="hidden" name="quantity" value="1" />
                        <input type="hidden" name="hidden_image" value="<?php echo $row["Item_image"];?>"/>
						<input type="hidden" name="hidden_name" value="<?php echo $row["Item_name"]; ?>" />
						<input type="hidden" name="hidden_price" value="<?php echo $row["Item_price"]; ?>" />
						<input type="hidden" name="hidden_id" value="<?php echo $row["ID"]; ?>" />
						<button type="submit" name="add_to_cart" data-toggle="tooltip" data-placement="left" style="background : none;" title="Add to Cart" value=""><img src="img/core-img/cart.png" alt=""></button>
					</div>
				</form>
			</div>
                
<?php
    }
    echo '</div>';
} else {
    echo "0 results";
}
// function addTocart($userID,$itemID){
   
// }

$conn->close();
?>
            </div>
        </div>
    </div>

   
    <!-- ##### Newsletter Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                            <a href="index.php"><img src="img/core-img/logo2.png" alt=""></a>
                        </div>
                        <!-- Copywrite Text -->
                        <p class="copywrite">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="fa fa-heart-o"
                                aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu">
                            <nav class="navbar navbar-expand-lg justify-content-end">
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#footerNavContent" aria-controls="footerNavContent"
                                    aria-expanded="false" aria-label="Toggle navigation"><i
                                        class="fa fa-bars"></i></button>
                                <div class="collapse navbar-collapse" id="footerNavContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="index.php">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="shop.php">Shop</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="product-details.php">Product</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="cart.php">Cart</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="checkout.php">Checkout</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
 
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    <script>
          
                $(".catagory").click(function(){
                console.log("click");
                $("this").addClass("active");
            });
           
           
    </script>
    

</body>

</html>